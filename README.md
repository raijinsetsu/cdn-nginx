# README #

Source repository for the CDN software stack.

### How do I get set up? ###
git clone git@bitbucket.org:raijinsetsu/cdn-nginx.git

cd cdn-nginx

docker-compose up --build -d